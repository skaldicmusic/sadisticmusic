// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <iostream>
#include <stdio.h>


// TODO: reference additional headers your program requires here

#include <math.h>
#include <string>
#include <iterator>
#include <fstream>
#include <process.h>
#include <vector>
#include <list>
#include <time.h>

#include "resource.h"
//#include <afxcmn.h>