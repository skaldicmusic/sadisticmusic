//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Soundv2.rc
//

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_SOUNDV2_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_SOUNDV2			107
#define IDI_SMALL				108
#define IDC_SOUNDV2			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif


/*
* some good values for block size and count
*/
#pragma once

#define BLOCK_SIZE  8192
#define BLOCK_COUNT 4						//if it sounds juddery increase this.
#define SAMPLE_RATE 44100
#define BIT_RATE 16							//This should be set to 16. increasing the bit rate will have no effect on quality. (see sound generation).	
#define BUFFER_SIZE 8192

#define PI 3.141592654				

#define A0 27.5						//Frequency of A0 (bottom A) in Hz
#define NO_OF_ADDRESSABLE_NOTES 109	//By default this should be 109. This will allow B8 to be played. There is not reason this cannot be increased. 







//ensure that the last line is blank.
