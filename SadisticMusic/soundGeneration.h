// contains declerations for functions in soundGeneration.cpp


bool fillBuffer(char* pbegin, char* pend);
short createSample();
double toneFreq(int toneID);
std::vector<short> get_tone();
int tone_compiler();
double getNoteInKey(int key, int octave, int note);