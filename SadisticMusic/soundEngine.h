#pragma once

#include <Windows.h>

/*
* function prototypes
*/
unsigned int __stdcall soundMain(void * data);
void CALLBACK waveOutProc(HWAVEOUT, UINT, DWORD, DWORD, DWORD);
static WAVEHDR* allocateBlocks(int size, int count);
void freeBlocks(WAVEHDR* blockArray);
void writeAudio(HWAVEOUT hWaveOut, LPSTR, int size);



