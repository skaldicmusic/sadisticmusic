//This code is mainly adapted from David Overtons tutorial "Playing Audio in Windows using waveOut Interface" which can be found at 
//http://www.planet-source-code.com/vb/scripts/ShowCode.asp?txtCodeId=4422&lngWId=3


#include "stdafx.h"
#include "projectHeaders.h"


//note - you must add winmm.lib to list of library files for this to compile.

unsigned int __stdcall soundMain(void* data);
void CALLBACK waveOutProc(HWAVEOUT, UINT, DWORD, DWORD, DWORD);
static WAVEHDR* allocateBlocks(int size, int count);
void freeBlocks(WAVEHDR* blockArray);
void writeAudio(HWAVEOUT hWaveOut, LPSTR, int size);
bool createSineWave(char* pbegin, char* pend);
short createSample();
int printStuff(const std::string &text);





/*
external declerations
*/
extern CRITICAL_SECTION messageDataCS;
extern bool soundStop;
extern bool soundExit;



/*
* module level variables
*/
CRITICAL_SECTION waveCriticalSection;
WAVEHDR* waveBlocks;
volatile int waveFreeBlockCount;
int waveCurrentBlock;


unsigned int __stdcall soundMain(void* data)
{
	HWAVEOUT hWaveOut;							//device handle
	WAVEFORMATEX wfx;							//wave format x structure.
	char buffer[BUFFER_SIZE];					//intermediate buffer for holding wave data.

												/*
												* initialise the module variables
												*/
	waveBlocks = allocateBlocks(BLOCK_SIZE, BLOCK_COUNT);
	waveFreeBlockCount = BLOCK_COUNT;
	waveCurrentBlock = 0;
	InitializeCriticalSection(&waveCriticalSection);

	/*
	* set up the WAVEFORMATEX structure.
	*/
	wfx.nSamplesPerSec = SAMPLE_RATE;			// sample rate in Hz
	wfx.wBitsPerSample = BIT_RATE;				//sample size in bits
	wfx.nChannels = 1;							// channels
	wfx.cbSize = 0;								// size of _extra_ info
	wfx.wFormatTag = WAVE_FORMAT_PCM;			//don't change this
	wfx.nBlockAlign = (wfx.wBitsPerSample * wfx.nChannels) / 8;  //don't change this
	wfx.nAvgBytesPerSec = wfx.nBlockAlign * wfx.nSamplesPerSec;//don't change this


	if (waveOutOpen						//trys to initialise default wave device.
	(
		&hWaveOut,
		WAVE_MAPPER,
		&wfx,
		(DWORD_PTR)waveOutProc,
		(DWORD_PTR)&waveFreeBlockCount,
		CALLBACK_FUNCTION
	) != MMSYSERR_NOERROR)
	{
		writeToLog("unable to open wave mapper device.\n");
		exit(1);
	}

	/*
	* playback loop
	*/
	EnterCriticalSection(&messageDataCS);
	bool localSoundStop = soundStop;
	bool localSoundExit = soundExit;
	LeaveCriticalSection(&messageDataCS);
																	//This is the section which generates sound.
	while (localSoundExit==0)
	{
		while (localSoundStop==0 && localSoundExit == 0)
		{

			if (!fillBuffer(std::begin(buffer), std::end(buffer)))

				writeAudio(hWaveOut, buffer, sizeof(buffer));

			EnterCriticalSection(&messageDataCS);
			localSoundStop = soundStop;
			localSoundExit = soundExit;
			LeaveCriticalSection(&messageDataCS);
		}

		/*
		* unprepare any blocks that are still prepared
		*/

		while (waveFreeBlockCount < BLOCK_COUNT)     //waits for blocks to become free.
			Sleep(10);

		EnterCriticalSection(&messageDataCS);
		localSoundStop = soundStop;
		localSoundExit = soundExit;
		LeaveCriticalSection(&messageDataCS);
	}


	for (int i = 0; i < waveFreeBlockCount; i++)
		if (waveBlocks[i].dwFlags & WHDR_PREPARED)
			waveOutUnprepareHeader(hWaveOut, &waveBlocks[i], sizeof(WAVEHDR));

	DeleteCriticalSection(&waveCriticalSection);
	freeBlocks(waveBlocks);										//clean up
	waveOutClose(hWaveOut);
	return 0;
}



void writeAudio(HWAVEOUT hWaveOut, LPSTR data, int size)
{
	WAVEHDR* current;
	int remain;
	current = &waveBlocks[waveCurrentBlock];
	while (size > 0) {
		/*
		* first make sure the header we're going to use is unprepared
		*/
		if (current->dwFlags & WHDR_PREPARED)											//unprepares header if it is prepared
			waveOutUnprepareHeader(hWaveOut, current, sizeof(WAVEHDR));
		if (size < static_cast<int>(BLOCK_SIZE - (current->dwUser)))						//lp data is a pointer to waveform data. (dwuser is a DWORD_PTR)
		{																				//This code may be unnecessary. It is dealing with the problem where the size of data won't fill the whole block.
			memcpy(current->lpData + current->dwUser, data, size);
			current->dwUser += size;
			break;
		}
		remain = BLOCK_SIZE - current->dwUser;
		memcpy(current->lpData + current->dwUser, data, remain);
		size -= remain;
		data += remain;																	//moves pointer up data.
		current->dwBufferLength = BLOCK_SIZE;
		waveOutPrepareHeader(hWaveOut, current, sizeof(WAVEHDR));
		waveOutWrite(hWaveOut, current, sizeof(WAVEHDR));								//sends data to aduio device.
		EnterCriticalSection(&waveCriticalSection);
		waveFreeBlockCount--;
		LeaveCriticalSection(&waveCriticalSection);

		while (!waveFreeBlockCount)														//wait here untill a block becomes free
			Sleep(10);
		/*
		* point to the next block
		*/
		waveCurrentBlock++;
		waveCurrentBlock %= BLOCK_COUNT;
		current = &waveBlocks[waveCurrentBlock];
		current->dwUser = 0;
	}
}

WAVEHDR* allocateBlocks(int size, int count)
{
	LPSTR buffer;
	int i;
	WAVEHDR* blocks;
	DWORD totalBufferSize = (size + sizeof(WAVEHDR)) * count;
	/*
	* allocate memory for the entire set in one go
	*/
	if ((buffer = static_cast<LPSTR>(HeapAlloc(
		GetProcessHeap(),
		HEAP_ZERO_MEMORY,
		totalBufferSize
	))) == NULL) {
		writeToLog("Memory allocation error\n");
		exit(1);
	}
	/*
	* and set up the pointers to each bit
	*/
	blocks = (WAVEHDR*)buffer;
	buffer += sizeof(WAVEHDR) * count;
	for (i = 0; i < count; i++) {
		blocks[i].dwBufferLength = size;
		blocks[i].lpData = buffer;
		buffer += size;
	}
	return blocks;
}
void freeBlocks(WAVEHDR* blockArray)
{
	/*
	* and this is why allocateBlocks works the way it does
	*/
	HeapFree(GetProcessHeap(), 0, blockArray);
}

static void CALLBACK waveOutProc(
	HWAVEOUT hWaveOut,
	UINT uMsg,
	DWORD dwInstance,
	DWORD dwParam1,
	DWORD dwParam2
)
{
	/*
	* pointer to free block counter
	*/
	int* freeBlockCounter = (int*)dwInstance;
	/*
	* ignore calls that occur due to openining and closing the
	* device.
	*/
	if (uMsg != WOM_DONE)
		return;
	EnterCriticalSection(&waveCriticalSection);
	(*freeBlockCounter)++;
	LeaveCriticalSection(&waveCriticalSection);
}




